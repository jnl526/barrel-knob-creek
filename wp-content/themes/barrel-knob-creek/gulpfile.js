var gulp = require('gulp')
var concat = require('gulp-concat')
var autoprefixer = require('gulp-autoprefixer')
var sass = require('gulp-sass')
var csso = require('gulp-csso')
var minify = require('gulp-minify')

// Gulp task to minify CSS files
gulp.task('styles', function () {
  return gulp.src('src/css/main.scss')
    // Compile SASS files
    .pipe(sass({
      outputStyle: 'nested',
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    // Auto-prefix css styles for cross browser compatibility
    .pipe(autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
    // Minify the file
    .pipe(csso())
    .pipe(concat('main.min.css'))
    // Output
    .pipe(gulp.dest('assets/'))
})

// Gulp task to minify JavaScript files
gulp.task('scripts', function () {
  return gulp.src('src/js/main.js')
    .pipe(minify())
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest('assets/'))
})

// Set the browser that you want to support
const AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
]
