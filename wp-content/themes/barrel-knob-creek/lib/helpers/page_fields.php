<?php
function my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'group_5b75b006a06cc',
		'title' => 'Hero Content',
		'fields' => array (
			array(
        'key' => 'field_5b75b06055f6f',
        'label' => 'Hero Content',
        'type' => 'tab',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'class' => 'main-title',
        ),
        'placement' => 'top',
        'endpoint' => 0,
      ),
      array(
        'key' => 'field_5b75d99ffe349',
        'label' => 'Title',
        'name' => 'title',
        'type' => 'text',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
      ),
      array(
        'key' => 'field_5b75d9cdfe34a',
        'label' => 'Content',
        'name' => 'content',
        'type' => 'textarea',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,

      ),
      array(
        'key' => 'field_5b75e6b99c539',
        'label' => 'Content Image',
        'name' => 'content_image',
        'type' => 'image',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,       
        'return_format' => 'url',
        'preview_size' => 'thumbnail',
        'library' => 'all',
      ),
    ),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'templates/page-flexible.php',
				),
			),
		),
	));
	
}

add_action('acf/init', 'my_acf_add_local_field_groups');