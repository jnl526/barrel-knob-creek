<!-- // Create Featured Article -->
<div class = "modal-bk"></div>
<div class="featured-post container">  
	
<!-- Query most recent post  -->
	
	<div class="wrap">
			<?php $query_args = array(
			'post_type' => array('article', 'recipe', 'video', 'gallery') ,
			'posts_per_page' => 1 ,
			'order' => 'DESC'
			);

		$query = new WP_Query( $query_args ); ?>
		
		<?php if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<div class="row">
						<div class="col col-2">
							<?php $image = get_the_post_thumbnail_url( $post->ID, 'large' );
							the_module('image', array(
								'image' => $image
							));
							?>
						</div>   

						<div class="col col-2">
							<div class="post-content">
								<div class="post-info">
									<div class="item">
										<i class= "<?=get_post_type()?>"></i>
										<div class="date"><?php the_date('F d') ?></div>
										<h3 class="main-heading" ><?php the_title(); ?></h3>
										<p class="excerpt"><?php echo get_the_excerpt(); ?></p>
										<a href="#" data-target ="postModal-<?php echo	get_the_ID() ?>" data-toggle="modal"> Read More</a> 
									</div>
								</div>
							</div>
						</div>   
					</div> 
					<?php 
	the_module('post_modal');
?>
				<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		<?php endif; ?>
		
	</div>
	 	
</div>
