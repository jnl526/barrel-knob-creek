<div class="page-content clearfix">
  <div class="container">
    <article class="post">
      <div class="entry-title">
      <i class= "<?=get_post_type()?>"></i>
      <p class="date"><?php echo get_the_date('F d') ?><p>
      <h1><?php the_title(); ?></h1>
      <p class="info">by <?php the_author(); ?><p>
    
      </div>
      <main>
        <div class="row clearfix">
        <?php the_content(); ?>
        <?php 
          $image = get_field('gallery');
          $id = wp_get_attachment_image( $image['url'] );
          $size = 'full'; // (thumbnail, medium, large, full or custom size)

          if( $image ): ?>
            <div class="gallery-grid">
              <?php foreach( $image as $image ): ?>
                <div class="item col col-2 col-3">
                  <div class="image-wrapper" >
                    <?php echo the_module('image', array(
              'image' => $image
            )); ?>
                  </div>
                </div>
                  <?php 
										the_module('gallery_modal');
									?>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>
      </main>
    </article>
  </div>
</div>