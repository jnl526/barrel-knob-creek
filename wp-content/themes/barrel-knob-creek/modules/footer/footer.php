<!--  Footer  -->
<?php
$image = get_field('logo_image', 'options');
?>
		<div class="container">
			<?php if ($image) : ?>
				<div class="logo-wrapper">
					<a href="<?php echo home_url( '/' ); ?>">
						<img class="logo" src="<?php echo $image; ?>" />
					</a>	
				</div>
			<?php else : ?>
				<div class="site-title">
							<h4>
							<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<?php bloginfo( 'name' ); ?>
								</a>
							</h4>
							<h4 class="sub-heading-small">
								<?php bloginfo( 'description' ); ?>
							</h4>
				</div>
			<?php endif; ?>

				<?php if( have_rows('social_media_links', 'options') ): ?>
					<nav class="social-links">
						<ul>
							<li>Share On</li>
							<?php while( have_rows('social_media_links', 'options') ): the_row(); ?>
								<li>
								<?php if( the_sub_field('label') ): ?>
									<a href="<?php echo the_sub_field('url'); ?>" target="_blank">
										<?php echo the_sub_field('label'); ?>
										<?php else : ?>
										<a href="<?php echo the_sub_field('url'); ?>" target="_blank">
											 <i	class= "fab fa-<?=the_sub_field('type')?>"></i>
										</a>
									 		<?php endif; ?>
									</a>
								</li>
							<?php endwhile; ?>
						</ul>
					</nav>
				<?php endif; ?>
		
		</div>	