<div id="postModal-<?php echo	get_the_ID() ?>" class="modal">
  <div class="modal-container">
    <p style="text-align: right; margin: 0; line-height: 10px;">  
        <span data-dismiss="modal" class="close">&times;</span>
    </p>
    <div class="modal-header">
      <i class= "<?=get_post_type()?>"></i>
      <p class="date"><?php echo get_the_date('F d') ?><p>
      <h1 class= "main-heading"><?php the_title(); ?></h1>
      <p class="info">by <?php the_author(); ?><p>
    </div>
      <!-- Modal Content -->
    <div class="modal-content clearfix" >
      <div class="col col-2">
        <?php
          $image = get_the_post_thumbnail_url( $post->ID, 'large' );
          the_module('image', array(
            'image' => $image
          ));
        ?>
      </div>
      <div class="col col-2">
        <?php the_content(); ?>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>  
</div>