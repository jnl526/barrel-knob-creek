<div id="galleryModal-<?php echo wp_get_attachment_image_src(  $attachment_id ); ?>" class="modal">
  <div class="modal-container">
    <p style="text-align: right; margin: 0; line-height: 10px;">  
        <span data-dismiss="modal" class="close">&times;</span>
    </p>
      <!-- Modal Content -->
    <div class="modal-content clearfix" >
    <?php the_module('image', array('image' => $image));?>
      <div class="modal-footer">
      </div>
    </div>
  </div>  
</div>