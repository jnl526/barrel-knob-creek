<!-- // Create Featured Article -->
<div class="entry-header">
	<h1 class="main-heading">Recent Articles</h1>
</div>
<article class="recent-post">  
	      <!-- Query most recent post  -->
	<div class="container">
		<div class="post-grid">
				<?php $query_args = array(
				'post_type' => array('article', 'recipe', 'video', 'gallery') ,
				'posts_per_page' => 10 ,
				'offset'=>1,
				'order' => 'DESC'
				);

			$query = new WP_Query( $query_args ); ?>
					
			<?php if ( $query->have_posts() ) : ?>
			
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					
							<div id="item" class="post-item col col-2 col-3">
								<div class="image-wrapper" >
									<?php $image = get_the_post_thumbnail_url( $post->ID, 'large' );
									the_module('image', array(
										'image' => $image
									));
									?>
								</div>
								<div class="post-content padding10">
									<i class= "<?=get_post_type()?>"></i>
									<div class="date"><?php the_date('F d') ?></div>
									<h3 class="sub-heading-2" ><?php the_title(); ?></h3>
									<?php if( get_post_type() == 'gallery' ): ?>
										<p><a href="<?php the_permalink(); ?>">View Gallery</a> </p>
									<?php elseif( get_post_type() == 'video' ): ?> 
										<p><a href="#" data-target ="postModal-<?php echo	get_the_ID() ?>" data-toggle="modal">Watch Video</a> </p>
										<?php else : ?> 
										<p><a href="#" data-target ="postModal-<?php echo	get_the_ID() ?>" data-toggle="modal">Read More</a> </p>
									<?php endif; ?>
									<?php 
										the_module('post_modal');
									?>
								</div>
								
							</div>   <!--  .col-3  -->
						
					<?php endwhile; ?>
				
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>
		</div>
	</div>
	</div>   <!--  .container  -->
</article>