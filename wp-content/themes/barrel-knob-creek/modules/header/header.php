
<?php
$topbar = get_field('topbar', 'options');
$image = get_field('logo_image', 'options');
?>

<!--  Add a topbar   -->
	<div class="topbar">
		<div class="container">
				<?php echo $topbar ?>
		</div>
	</div>
	
<!--  Header  -->
	<header id="header" data-module="header">
		<div class="container">
			<?php if ($image) : ?>
				<div class="logo-wrapper">
					<a href="<?php echo home_url( '/' ); ?>">
						<img class="logo" src="<?php echo $image; ?>" />
					</a>	
				</div>
				<div class="site-title center">
					<h2>
							<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							Barrel
							</a>
						</h2>
				</div>
			<?php else : ?>
				<div class="site-title">
						<?php if ( is_front_page() ) : ?>
							<h2>
							<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<?php bloginfo( 'name' ); ?>
								</a>
							</h2>
							<h4 class="sub-heading-small">
								<?php bloginfo( 'description' ); ?>
							</h4>
						<?php else : ?>
							<h2>
								<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<?php bloginfo( 'name' ); ?>
								</a>
							</h2>
							<h4 class="sub-heading-small">
								<?php bloginfo( 'description' ); ?>
							</h4>
						<?php endif; ?>
				</div>
			<?php endif; ?>
			<div class="menu-bar">
				<?php
				wp_nav_menu( array(
					'container' => 'nav'
				) );
				?>

				<?php if( have_rows('social_media_links', 'options') ): ?>
					<nav class="social-links">
						<ul>
							<?php while( have_rows('social_media_links', 'options') ): the_row(); ?>
								<li>
								<?php if( the_sub_field('label') ): ?>
									<a href="<?php echo the_sub_field('url'); ?>" target="_blank">
										<?php echo the_sub_field('label'); ?>
										<?php else : ?>
										<a href="<?php echo the_sub_field('url'); ?>" target="_blank">
											 <i	class= "fab fa-<?=the_sub_field('type')?>"></i>
										</a>
									 		<?php endif; ?>
									</a>
								</li>
							<?php endwhile; ?>
						</ul>
					</nav>
				<?php endif; ?>
			</div> <!--  menu-bar -->
		
		</div>	<!--  container -->
	</header>