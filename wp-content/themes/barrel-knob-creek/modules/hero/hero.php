<section class="hero" data-module="hero">
<!-- // Add Hero Content -->
		<div class="content full-height">
		<?php
		$image = get_the_post_thumbnail_url( $post->ID, 'large' );
		the_module('image', array(
			'image' => $image
		));
?>
			<div class="container">
				<div class="row">
					<div class="col col-2">
						<div class="info">
							<h1 class="main-title"><?php the_field('title'); ?></h1>
							<p class="description"><?php the_field('content'); ?></p>
						</div>
					</div>
					<div class="col col-2">
						<div class="hero-img"> 
							<?php
									$image = get_field('content_image', $post_id, 'large');
									the_module('image', array(
										'image' => $image
									));
							?>
						</div>
					</div>
				</div>
				</div>
		</div>
		<?php the_module('featured_post');?>
</section>
