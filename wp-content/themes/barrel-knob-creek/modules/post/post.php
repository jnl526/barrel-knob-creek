<div class="page-content clearfix">
  <div class="container">
    <article class="post">
      <div class="entry-title">
      <i class= "<?=get_post_type()?>"></i>
      <p class="date"><?php echo get_the_date('F d') ?><p>
      <h1><?php the_title(); ?></h1>
      <p class="info">by <?php the_author(); ?><p>
    
      </div>
      <main>
        <div class="row clearfix">
          
          <div class="col-2">
            <?php
              $image = get_the_post_thumbnail_url( $post->ID, 'medium' );
              the_module('image', array(
                'image' => $image
              ));
          ?>
          </div>
          <div class="col-2">
            <?php the_content(); ?>
          </div>
      </main>
    </article>
  </div>
</div>