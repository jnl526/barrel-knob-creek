<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @package WordPress
 * @subpackage Barrel_Knob_Creek
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<footer class="clearfix">
			<div class="wrap">
        <?php
        the_module('footer');
				?>
			</div><!-- .wrap -->
		</footer>
	
<?php wp_footer(); ?>

</body>
</html>
