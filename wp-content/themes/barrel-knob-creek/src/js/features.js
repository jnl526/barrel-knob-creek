// Mouseover post items
window.onload = function () {
  var item = document.getElementsByClassName('post-item')
  // var image = document.getElementsByClassName('image-wrapper')
  for (let i = 0; i < item.length; i++) {
    item[i].addEventListener('mouseover', function () {
      item[i].classList.add('active')
    }, false)

    item[i].addEventListener('mouseout', function () {
      item[i].classList.remove('active')
    }, false)
  }
}

// Sticky Header
window.onscroll = function scroll () {
  var header = document.getElementById('header')
  var sticky = header.offsetTop

  if (window.pageYOffset > sticky) {
    header.classList.add('sticky')
  } else {
    header.classList.remove('sticky')
  }
}

// Display modal
document.addEventListener('click', function (event) {
  event = event || window.event
  var modalBK = document.querySelector('.modal-bk')
  var featurePost = document.querySelector('.featured-post')
  var target = event.target || event.srcElement
  if (target.hasAttribute('data-toggle') && target.getAttribute('data-toggle') === 'modal') {
    if (target.hasAttribute('data-target')) {
      var modalID = target.getAttribute('data-target')

      document.getElementById(modalID).classList.add('open')
      modalBK.classList.add('open')
      featurePost.style = 'overflow: visible'
      event.preventDefault()
    }
  }

  // Close modal
  if ((target.hasAttribute('data-dismiss') && target.getAttribute('data-dismiss') === 'modal') || target.classList.contains('modal')) {
    var modal = document.querySelector('[class="modal open"]')
    modal.classList.remove('open')
    modalBK.classList.remove('open')
    featurePost.style = 'overflow: hidden'
    event.preventDefault()
  }
}, false)
