<?php
  /* 
  Template Name: Flexible 
  Template Post Type: post, page
  */
  get_header();
  ?>
    <div class="page-content clearfix">
      
      <?php 
        the_module('hero');
        ?>
     <section class="main-section">
        <?php 
          if ( is_front_page() ) : 
            
            the_module('recent_posts');
            
          elseif ( have_posts()  ):
            the_post();
            the_module('post');
          endif; 
      ?>
      </section>
    </div>
  <?php get_footer() ?>
  
